package org.reisal78.atm.service;

import org.reisal78.atm.model.entity.Account;
import org.reisal78.atm.model.entity.Bank;
import org.reisal78.atm.model.entity.Card;
import org.reisal78.atm.model.entity.Client;
import org.reisal78.atm.model.repository.BankRepository;
import org.reisal78.atm.model.repository.BankRepositoryImpl;

import java.util.*;

public class BankServiceImpl implements BankService {

    private BankRepository bankRepository = new BankRepositoryImpl();

    @Override
    public void addNewBank(String name, String BIN) {

        // TODO: 26.01.17 проверить параметры

        Bank bank = new Bank(name, BIN);
        bankRepository.save(bank);
    }

    @Override
    public void addNewClient(String bin, String clientName) {
        Bank bank = bankRepository.findByBIN(bin);
        bank.getClients().add(new Client(clientName));
        bankRepository.save(bank);
    }

    @Override
    public void addClientAccount(String bin, String clientName) {
        Bank bank = bankRepository.findByBIN(bin);
        Client client = findClient(bank, clientName);
        bank.getAccounts().put(client, new Account());
        bankRepository.save(bank);
    }

    private Client findClient(Bank bank, String clientName) {
        for (Client client : bank.getClients()) {
            if (client.getName().equals(clientName)) {
                return client;
            }
        }
        return null;
    }

    @Override
    public void createClientCard(String bin, String clientName) {
        Bank bank = bankRepository.findByBIN(bin);
        Client client = findClient(bank, clientName);
        Card card = new Card();
        String cardNumber = generateCardNumber(bank.getBIN());
        card.setNumber(cardNumber);
        bank.getCards().put(client, card);
        bank.getPins().put(card, generatePin());
        bankRepository.save(bank);
    }

    private String generatePin() {
        Random r = new Random();
        int low = 1000;
        int high = 9999;
        return String.valueOf(r.nextInt(high - low) + low);
    }

    @Override
    @Deprecated
    public List<String> getAllBanks() {
        Collection<Bank> banks = bankRepository.getAllBanks();
        List<String> bankNames = new ArrayList<>();
        for (Bank bank :banks) {
            bankNames.add(bank.getName());
        }
        return bankNames;
    }

    @Override
    @Deprecated
    public List<String> getBankClients(String bin) {
        Bank bank = bankRepository.findByBIN(bin);
        List<String> clients = new ArrayList<>();
        for (Client client : bank.getClients()) {
            clients.add(client.getName());
        }
        return clients;
    }

    @Override
    @Deprecated
    public List<String> getAllCards(String bin) {
        Bank bank = bankRepository.findByBIN(bin);
        List<String> cards = new ArrayList<>();
        for (Map.Entry<Card, String> clientCardEntry : bank.getPins().entrySet()) {
            cards.add(clientCardEntry.getKey().getNumber() + " - " + clientCardEntry.getValue());
        }
        return cards;
    }

    @Override
    public boolean validateCard(String cardNumber, String pin) {
        return true;
    }

    private String generateCardNumber(String bin) {
        Random r = new Random();
        int low = 1000000;
        int high = 9999999;
        StringBuilder sb = new StringBuilder();
        sb.append(bin);
        sb.append(r.nextInt(high - low) + low);
        sb.append(r.nextInt(9 - 0) + 0);
        return sb.toString();
    }
}
