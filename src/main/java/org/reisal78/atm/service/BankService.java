package org.reisal78.atm.service;

import java.util.List;

public interface BankService {
    void addNewBank(String name, String BIN);

    void addNewClient(String bin, String clientName);

    void addClientAccount(String bin, String clientName);

    void createClientCard(String bin, String client);

    @Deprecated
    List<String> getAllBanks();

    @Deprecated
    List<String> getBankClients(String bin);

    @Deprecated
    List<String> getAllCards(String bin);

    boolean validateCard(String cardNumber, String pin);
}
