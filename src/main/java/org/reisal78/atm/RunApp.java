package org.reisal78.atm;

import org.reisal78.atm.service.BankService;
import org.reisal78.atm.service.BankServiceImpl;
import org.reisal78.atm.ui.controller.MainController;
import org.reisal78.atm.ui.view.MainView;
import org.reisal78.atm.ui.view.MainViewImpl;

import java.util.List;

public class RunApp {

    public static void main(String[] args) {

        //initial data
        final String SBERBANK_BIN = "432453";
        final String VTB24_BIN = "343243";
        final String CLIENT1 = "Иван Иванов";
        final String CLIENT2 = "Петр Петров";

        BankService bankService = new BankServiceImpl();
        bankService.addNewBank("Сбербанк", SBERBANK_BIN);
        bankService.addNewBank("ВТБ24", VTB24_BIN);

        bankService.addNewClient(SBERBANK_BIN, CLIENT1);
        bankService.addNewClient(VTB24_BIN, CLIENT2);

        bankService.addClientAccount(SBERBANK_BIN, CLIENT1);
        bankService.addClientAccount(VTB24_BIN, CLIENT2);

        bankService.createClientCard(SBERBANK_BIN, CLIENT1);
        bankService.createClientCard(VTB24_BIN, CLIENT2);

        List<String> banks = bankService.getAllBanks();
        System.out.println(banks);

        List<String> clients;
        clients = bankService.getBankClients(SBERBANK_BIN);
        System.out.print("Клиенты Сбербанка: ");
        System.out.println(clients);

        clients = bankService.getBankClients(VTB24_BIN);
        System.out.print("Клиенты ВТБ24: ");
        System.out.println(clients);

        List<String> cards;
        cards = bankService.getAllCards(SBERBANK_BIN);
        System.out.print("Карты Сбербанка: ");
        System.out.println(cards);

        cards = bankService.getAllCards(VTB24_BIN);
        System.out.print("Карты ВТБ24: ");
        System.out.println(cards);
        //end initial

        MainController mainController = new MainController(bankService);
        MainView mainView = new MainViewImpl(mainController);


    }


}
