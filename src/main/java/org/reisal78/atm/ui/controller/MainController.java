package org.reisal78.atm.ui.controller;

import org.reisal78.atm.service.BankService;
import org.reisal78.atm.ui.view.MainView;
import org.reisal78.atm.ui.view.MainViewImpl;

public class MainController {

    private final BankService bankService;
    private MainView mainView;

    public MainController(BankService bankService) {
        this.bankService = bankService;
    }

    public MainView getMainView() {
        return mainView;
    }

    public void setMainView(MainView mainView) {
        this.mainView = mainView;
    }

    public void insertCardEvent(String cardNumber, String pin) {
        if (bankService.validateCard(cardNumber, pin)) {
            mainView.showMainMenu();
        }
    }

    public void eventGetBalance() {

    }
}
