package org.reisal78.atm.ui.view;

import org.reisal78.atm.ui.controller.MainController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainViewImpl implements MainView {

    private MainController controller;

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public MainViewImpl(MainController mainController) {
        this.controller = mainController;
        controller.setMainView(this);
        showInsertCardPrompt();
    }

    @Override
    public void setController(MainController mainController) {
        this.controller = mainController;
    }

    @Override
    public void showMainMenu() {
        System.out.println("1. Показать баланс");
        System.out.println("2. Снять наличные");
        System.out.println("0. Выход");

        int choice;
        while ((choice = inputInteger("Выберите пукнт")) != 0) {
            switch (choice) {
                case 1:
                    controller.eventGetBalance();
                    break;
                default:
                    System.out.println("Unknown choice");
            }
        }

    }

    public void showInsertCardPrompt() {
        System.out.println("Insert a card...");
        String cardNumber = inputString("Введите номер карты: ");
        String pin = inputString("Введите pin");
        controller.insertCardEvent(cardNumber, pin);
    }

    private String inputString(String prompt) {
        while (true) {
            try {
                System.out.print(prompt + ": ");
                return reader.readLine().trim();
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    private int inputInteger(String prompt) {
        while (true) {
            try {
                System.out.print(prompt + ": ");
                return Integer.parseInt(reader.readLine());
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input");
            }
        }
    }

}
