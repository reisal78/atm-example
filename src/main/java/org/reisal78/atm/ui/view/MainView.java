package org.reisal78.atm.ui.view;

import org.reisal78.atm.ui.controller.MainController;

public interface MainView {

    void setController(MainController mainController);

    void showMainMenu();
}
