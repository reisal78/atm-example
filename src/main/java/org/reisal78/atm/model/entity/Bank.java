package org.reisal78.atm.model.entity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Bank {

    private String name;
    private String BIN;
    private Set<Client> clients;
    private Map<Client, Account> accounts;
    private Map<Client, Card> cards;
    private Map<Card, String> pins;

    public Bank(String name, String BIN) {
        this.name = name;
        this.BIN = BIN;
        clients = new HashSet<>();
        accounts = new HashMap<>();
        cards = new HashMap<>();
        pins = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBIN() {
        return BIN;
    }

    public void setBIN(String BIN) {
        this.BIN = BIN;
    }

    public Set<Client> getClients() {
        return clients;
    }

    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }

    public Map<Client, Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Map<Client, Account> accounts) {
        this.accounts = accounts;
    }

    public Map<Client, Card> getCards() {
        return cards;
    }

    public void setCards(Map<Client, Card> cards) {
        this.cards = cards;
    }

    public Map<Card, String> getPins() {
        return pins;
    }

    public void setPins(Map<Card, String> pins) {
        this.pins = pins;
    }
}
