package org.reisal78.atm.model.repository;

import org.reisal78.atm.model.entity.Bank;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class BankRepositoryImpl implements BankRepository {

    private Set<Bank> banks = new HashSet<>();

    @Override
    public void save(Bank bank) {
        banks.add(bank);
    }

    @Override
    public Bank findByBIN(String bin) {
        for (Bank bank : banks) {
            if (bank.getBIN().equals(bin)) {
                return bank;
            }
        }
        return null;
    }

    @Override
    public Collection<Bank> getAllBanks() {
        return banks;
    }

}
