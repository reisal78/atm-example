package org.reisal78.atm.model.repository;

import org.reisal78.atm.model.entity.Bank;
import org.reisal78.atm.model.entity.Client;

import java.util.Collection;

public interface BankRepository {
    void save(Bank bank);

    Bank findByBIN(String bin);

    Collection<Bank> getAllBanks();
}
